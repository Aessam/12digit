﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using _12DigitID.Data.Repo.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Npgsql;

namespace _12DigitID.Controllers
{

    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class MainController : ControllerBase
    {
        private static Random rng = new Random(Environment.TickCount);
        private readonly IIdentityNumberRepository _identityNumberRepository;

        public MainController(IIdentityNumberRepository identityNumberRepository)
        {
            _identityNumberRepository = identityNumberRepository;
        }


        [Route("api/Generate12digitID")]
        [HttpGet]
        public async Task<IActionResult> GenerateID()
        {
            try
            {

                long number = 0;
                try
                {
                    number = Convert.ToInt64(GetNewID());
                }
                catch (Exception)
                {

                    return StatusCode(500, "error in creating new ID");
                }


                _identityNumberRepository.Add(new Entity.IdentityNumbers { IdentityNumber = Convert.ToInt64(number), CreationDate = DateTime.Now });
                await _identityNumberRepository.SaveAsync();

                return Ok(number);

            }
            catch (DbUpdateException ex)
            {
                if (ex.GetBaseException() is PostgresException pgException)
                {
                    if (pgException.SqlState == "23505")
                    {
                        long number = 0;
                        try
                        {
                            number = Convert.ToInt64(GetNewID());
                        }
                        catch (Exception e)
                        {

                            return StatusCode(500, "error in creating new ID");
                        }


                        _identityNumberRepository.Add(new Entity.IdentityNumbers { IdentityNumber = Convert.ToInt64(number), CreationDate = DateTime.Now });
                        await _identityNumberRepository.SaveAsync();

                        return Ok(number);
                    }
                    else
                    {
                        return StatusCode(500, pgException.SqlState);
                    }

                }
                else
                {
                    return StatusCode(500);
                }
            }
            catch (Exception e)
            {
                return StatusCode(500, e.ToString());
            }
         
        }

        private string GetNewID()
        {
            var number = "100400353363";
            for (int index = 0; index < 20; index++)
            {
                int length = Convert.ToInt32(12);
                number = rng.NextDouble().ToString("0.000000000000").Substring(2, length);
            }
            return number;
        }
    }
}