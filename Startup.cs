using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _12DigitID.Data;
using _12DigitID.Data.ConfigClasses;
using _12DigitID.Data.Repo;
using _12DigitID.Data.Repo.Interfaces;
using _12DigitID.Utilities;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;


namespace _12DigitID
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddScoped<_12DigitDbContext>();
            services.AddTransient<IIdentityNumberRepository, IdentityNumberRepository>();

            services.ConfigurePOCO<Config>(Configuration.GetSection("Config"));

            #region Authentication
            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                    .AddIdentityServerAuthentication(o =>
                    {

                        o.Authority = Config.IdentityAuthorizeUrl;
                        o.EnableCaching = true;
                        o.CacheDuration = TimeSpan.FromMinutes(10);
                        o.RequireHttpsMetadata = false;
                        o.ApiName = "12Digit";

                       
                    });

       

            #endregion

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseAuthentication();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
